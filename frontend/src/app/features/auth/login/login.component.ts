import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, UntypedFormGroup, FormGroup, FormControl, AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { Country } from 'src/app/core/types/countries.model';
import { Province } from 'src/app/core/types/province.model';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
    loginForm!: UntypedFormGroup;
    loading!: boolean;
    isSecondStepActivated!: boolean;
    countries!: Country[];
    currentProvinces!: Province[];

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService) {
    }

    ngOnInit() {
        this.authenticationService.logout();
        this.createForm();
        this.getCountries();
    }

    getCountries() {
        this.authenticationService
            .getCountries()
            .subscribe(
                data => {
                    this.countries = data;
                    this.currentProvinces = data[0].provinces
                }
            );
    }

    choseCountry(event: any) {
        this.currentProvinces = this.countries.find(x => x.id == event.value)?.provinces as Province[];
    }

    private createForm() {
        this.loginForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required),
            repeatPassword: new FormControl('', Validators.required),
            countryId: new FormControl('', Validators.required),
            provinceId: new FormControl('', Validators.required),
        }, [CustomValidators.MatchValidator('password', 'repeatPassword')]);
    }


    get passwordMatchError() {
        return (this.loginForm.getError('mismatch') && this.loginForm.get('repeatPassword')?.touched)
    }

    register() {
        this.loading = true;
        this.authenticationService
            .register(this.loginForm)
            .subscribe(
                data => {
                    alert(`Пользователь ${data} зарегестрирован`)
                    this.loading = false;
                    this.router.navigate(['/auth/login']);
                },
                error => {
                    alert(error.message);
                }
            );
    }

    nextStep() {
        this.isSecondStepActivated = true;
    }
}


export class CustomValidators {
    static MatchValidator(source: string, target: string): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            const sourceControl = control.get(source);
            const targetControl = control.get(target);

            return sourceControl && targetControl && sourceControl.value !== targetControl.value ? { mismatch: true } : null;
        };
    }
}
