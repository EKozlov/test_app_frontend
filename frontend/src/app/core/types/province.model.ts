export interface Province {
    id: string;
    createdDate: string;
    name: string;
    countryId: string;
}