import { Province } from "./province.model";

export interface Country {
    name: string;
    id: string;
    provinces: Province[]
}