import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { FormGroup } from '@angular/forms';
import { Country } from '../types/countries.model';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    constructor(private httpClient: HttpClient,
        @Inject('LOCALSTORAGE') private localStorage: Storage) {
    }

    register(form: FormGroup) {
        return this.httpClient.post(`${environment.apiBaseUri}api/User/register`, JSON.stringify(form.value),
            {
                headers: {
                    'content-type': 'application/json'
                }
            }
        )
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.localStorage.removeItem('access_token');
    }

    getCountries() {
        return this.httpClient.get<Country[]>(`${environment.apiBaseUri}api/User/countries`)
    }
}
